---
title: patdavid-check-layer
author: Pat David <patdavid@gmail.com>
license: GPLv2+
date: 2016-04-01
---

This script will automate the creation of blue-channel check layers for skin retouching.
